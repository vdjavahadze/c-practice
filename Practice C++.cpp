﻿#include <iostream>
#include <string>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "voice!" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!" << "\n";
    }
};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Woof!" << "\n";
    }
};

class Cow : public Animal
{
    void Voice() override
    {
        std::cout << "Moo!" << "\n";
    }
};

int main()
{
    Animal* cat = new Cat;
    Animal* dog = new Dog;
    Animal* cow = new Cow;

    Animal* farm[3]{ cat, dog, cow };
    for (int i = 0; i < 3; i++)
    {
        farm[i]->Voice();
    }
}